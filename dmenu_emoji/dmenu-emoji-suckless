#!/usr/bin/env bash
#
# Script name: dm-emoji
# Description: Copy an emoji to your clipboard using dmenu.
# Dependencies: dmenu, xclip
# GitLab: https://www.gitlab.com/azureorange404/dmenu-scripts
# License: https://www.gitlab.com/azureorange404/dmenu-scripts/LICENSE
# Forked from : dwt1

# Set with the flags "-e", "-o pipefail" cause the script to fail
# if certain things happen, which is a good thing.  Otherwise, we can
# get hidden bugs that are hard to discover.
set -euo pipefail

_path="$(cd "$(dirname "${BASH_SOURCE[0]}")" && cd "$(dirname "$(readlink "${BASH_SOURCE[0]}" || echo ".")")" && pwd)"
if [[  -f "${_path}/_dm-helper.sh" ]]; then
  # shellcheck disable=SC1090,SC1091
  source "${_path}/_dm-helper.sh"
else
  # shellcheck disable=SC1090
  echo "No helper-script found"
fi

# script will not hit this if there is no config-file to load
# shellcheck disable=SC1090
source "$(get_config)"


main() {
  # As this is loaded from other file it is technically not defined
  # shellcheck disable=SC2154
  selected="$(cat $HOME/.config/dmscripts/unicode/emoji_suckless | ${DMENU} "emoji:" "$@")"
  # selected="$(printf '%s\n' "${!EMOJI[@]}" | ${DMENU} "emoji:" "$@")"
  [ -z "${selected}" ] && exit 1
  echo "${EMOJI["${selected}"]}" | cp2cb
  # echo ${selected:0:1} | cp2cb
}

[[ "${BASH_SOURCE[0]}" == "${0}" ]] && main "$@"
