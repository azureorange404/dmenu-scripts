#!/bin/bash
# 
# Name:         dm-scan
# Description:  scanner interface for SANE (scanimage)
# Dependencies: dmenu, sane
# Gitlab:       https://gitlab.com/azureorange404/dmenu-scripts
# License:      https://gitlab.com/azureorange404/dmenu-scripts/LICENSE
#

set -euo pipefail

_path="$(cd "$(dirname "${BASH_SOURCE[0]}")" && cd "$(dirname "$(readlink "${BASH_SOURCE[0]}" || echo ".")")" && pwd)"
_path="$HOME/.local/bin"
if [[  -f "${_path}/_dm-helper.sh" ]]; then
  # shellcheck disable=SC1090,SC1091
  source "${_path}/_dm-helper.sh"
else
  # shellcheck disable=SC1090
  echo "No helper-script found"
fi

# script will not hit this if there is no config-file to load
# shellcheck disable=SC1090
source "$(get_config)"

function scanner {
    notify-send "looking for scanners ..."
    local _devices=$(scanimage -L | cut -d '`' -f 2 | cut -d "'" -f 1)
    _device=$(printf "%s\n" ${_devices[*]} | ${DMENU} "select device:")
}

function format {
    local _formats=("pnm" "tiff" "png" "jpeg" "pdf")
    _format=$(printf "%s\n" ${_formats[*]} | ${DMENU} "select format:")
}

function filebrowser {
    local _dir_list=("select" ".." $(echo */))
    local _selection=$(printf "%s\n" ${_dir_list[*]} | ${DMENU} "save to:")

    if [ $_selection == "select" ]; then
        _dir=$(pwd)
        return
    else
        cd "$_selection"
        filebrowser
    fi
}

function destination {
    local _options=("default" "current" "browse")
    local _choice=$(printf "%s\n" ${_options[*]} | ${DMENU} "save to:")

    case $_choice in
        default)
            _dir="$HOME";;
        current)
            _dir="$(pwd)";;
        browse)
            filebrowser;;
        *)
            _destination="hello";;
        esac
}

function name {
    _name=$(echo "" | ${DMENU} "file name:")
}

function edit {
    _options=()
    _choice=$(printf "%s\n%s\n\n%s" "device:  $_device" "save as: $_path/$_name.$_format" "scan" | ${DMENU} "preferences:")

    case $_choice in
        "device:"*)
            scanner
            edit;;
        "save as:"*)
            destination
            name
            format
            edit;;
        "scan")
            return;;
        *)
            edit;;
    esac
}

function main {

    scanner
    
    _path="$HOME"
    _name="scan"
    _format="pdf"

    edit

    notify-send "scanning to $_dir/$_name.$_format ..."
    scanimage --progress --device "$_device" --resolution 600 --format=$_format --output-file "$_dir/$_name.$_format"
}

main
