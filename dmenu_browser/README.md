# dm-browser

A script to use with xdg-open.

Select which browser to run, when opening a url.

## installation

`cp dm-browser.desktop ~/.local/share/applications/`

`xdg-settings set default-web-browser dm-browser.desktop`

Patch dmenu to include my -no-escape flag to avoid xdg-open running a random browser when hitting Escape on the dmenu prompt.
