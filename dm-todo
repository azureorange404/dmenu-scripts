#!/bin/bash
# 
# Name:         dm-todo
# Description:  task (todo) manager
# Dependencies: dmenu, todoman, vdirsyncer
# Gitlab:       https://gitlab.com/azureorange404/dmenu-scripts
# License:      https://gitlab.com/azureorange404/dmenu-scripts/LICENSE
#

set -euo pipefail

_path="$(cd "$(dirname "${BASH_SOURCE[0]}")" && cd "$(dirname "$(readlink "${BASH_SOURCE[0]}" || echo ".")")" && pwd)"
_path="$HOME/.local/bin"
if [[  -f "${_path}/_dm-helper.sh" ]]; then
  # shellcheck disable=SC1090,SC1091
  source "${_path}/_dm-helper.sh"
else
  # shellcheck disable=SC1090
  echo "No helper-script found"
fi

# script will not hit this if there is no config-file to load
# shellcheck disable=SC1090
source "$(get_config)"

clear_() {
    selected=""
    id=""
}

sync_() {
    if [ $# -eq 0 ]; then
        notify-send -u normal "syncing database"
        vdirsyncer sync &
    elif [[ "$1" = "-n" ]]; then
        notify-send -u normal "syncing database"
        vdirsyncer sync && notify-send -u normal "database synchronized" &
    fi
}

todo_loop() {
    if ! [[ "$selected" ]]; then
        options=(
            "new task"
            "list"
            "list completed"
            "sync"
            )
        choice=$(printf "%s\n" "${options[@]}" | ${DMENU} 'ToDo: ' "$@")
    elif ! [[ "$selected" == "back" ]]; then
        options=(
            "edit"
            "show details"
            "back"
            )
        if [[ -n $(echo "$selected" | grep "[X]") ]]; then
            # options=("mark not done" "${options[@]}")
            options=$options
        else
            options=("mark done" "${options[@]}")
        fi
        choice=$(printf "%s\n" "${options[@]}" | ${DMENU} 'Do: ' "$@")
    else
        clear_
        todo_loop
    fi

    case "$choice" in
        'new task')
            st -T "todo" -e todo new
            vdirsyncer sync &
        ;;
        'list')
            list=$(todo list --sort due --no-reverse)
            selected=$(echo -ne "${list[@]}\nback" | ${DMENU} 'tasks')
            id=$(echo $selected | cut -d "]" -f 2 | sed -e s/\ // | cut -d " " -f 1)
            todo_loop
        ;;
        'list completed')
            list=$(todo list --status COMPLETED --sort due)
            selected=$(echo -ne "${list[@]}\nback" | ${DMENU} 'tasks')
            id=$(echo $selected | cut -d "]" -f 2 | sed -e s/\ // | cut -d " " -f 1)
            todo_loop
        ;;
        'edit')
            st -T "todo" -e todo edit $id
            clear_
            todo_loop
        ;;
        'show details')
            detail=$(todo show $id)
            notify-send $detail
            todo_loop
        ;;
        'mark done')
            todo done $id
            clear_
            sync_
            todo_loop
        ;;
        'mark not done')
            todo done $id
            clear_
            sync_ -n
            todo_loop
        ;;
        'back')
            clear_
            todo_loop
        ;;
        'sync')
            sync_ -n
            ;;
        *)
            sync_
            exit 0
        ;;
    esac
}

main() {
    # Picking our options.
    selected=""
    todo_loop
}

[[ "${BASH_SOURCE[0]}" == "${0}" ]] && main "$@"
